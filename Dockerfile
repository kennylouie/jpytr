FROM registry.cto.ai/official_images/bash:latest as dep

FROM python:3.8.2-slim

COPY --from=dep /usr/local/bin/bash-sdk /bin/.
COPY --from=dep /bin/sdk-daemon /bin/.
COPY --from=dep /bin/interaction-server /bin/.

WORKDIR /ops

RUN apt update && apt install -y jq

RUN pip install jupyter

# add user specific deps
RUN pip install pandas numpy sklearn matplotlib

ADD jpytr.sh .
