#!/bin/bash

set -u

main()
{
	need_cmd bash-sdk
	need_cmd jq
	need_cmd stdbuf
	need_cmd jupyter

	local _dir _fln
	get_dir
	get_fln

	ensure cd "${SDK_STATE_DIR}/${_dir}" && ensure jupyter nbconvert --to script --execute "${_fln}"

	ensure stdbuf -oL python "${_fln/.ipynb/.py}"  | \
		while IFS= read -r line
		do
			say "${line}"
		done

	say Done!
}

get_dir()
{
	_dir="$(bash-sdk prompt input -a \
		--message "Directory name" \
		--name "dir" | jq --raw-output ".dir")"

	check_empty "${_dir}"
}

get_fln()
{
	_fln="$(bash-sdk prompt input -a \
		--message "notebook filename" \
		--name "file" | jq --raw-output ".file")"

	check_empty "${_fln}"
}

ensure()
{
	if ! "$@"; then err "command failed: $*"; fi
}

check_empty()
{
	[ -n "$1" ] || err "cannot be empty"
}

# utility function to indicate to user that program is needed
need_cmd()
{
	if ! check_cmd "$1"; then
		err "need $1 (command not found)"
	fi
}

# utility function to check if a program exectuable is callable
check_cmd()
{
	type "$1" >/dev/null 2>&1
}

# utility function to echo error and exit program
err()
{
	say "$@" >&2
	exit 1
}

# utility function to print with a tagline
say()
{
	bash-sdk print "$(printf "%b" "@jpytr: $@\n")"
}

main "$@"
