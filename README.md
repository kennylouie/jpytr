# jpytr op

This @cto.ai/op enters a directory inside the shared state directory shared between ops in a @cto.ai/op workflow and executes a jupyter notebook as a python script in terminal. The stdout will be displayed to the op interface.

This op can be run in the cli (using the @cto.ai tool https://cto.ai/docs/getting-started) or over slack (with the @cto.ai slackbot https://cto.ai/docs/slackapp)

# To use in a workflow:
+ Add to the ops.yml steps e.g:
```
steps:
	- ops run @kennylouie/jpytr:[LATEST_VERSION] --dir [DIRECTORY] --file [PATH_TO_JUPYTER_FILE]
	- ...

```
# Building from source and running

The value of the op comes from being used with a user-specific case. Add into the `Dockerfile` any deps needed by your make command.
E.g.
```
RUN: pip install scikit-learn
```

+ clone this repo
+ ensure @cto.ai/ops is installed
+ build the op
```
ops build .
```

+ publish the op to your team that is associated with the slackbot
```
ops publish .
```
